# Triangle Project Code.

# Triangle analyzes the lengths of the sides of a triangle
# (represented by a, b and c) and returns the type of triangle.
#
# It returns:
#   :equilateral  if all sides are equal
#   :isosceles    if exactly 2 sides are equal
#   :scalene      if no sides are equal
#
# The tests for this method can be found in
#   about_triangle_project.rb
# and
#   about_triangle_project_2.rb
#
def triangle(a, b, c)
  sides = [a, b, c].sort
  sum_of_shortest = sides[0] + sides[1]
  longest = sides[2]
  if sum_of_shortest <= longest
    raise TriangleError, "#{sum_of_shortest} <= #{longest}}"
  end
  n_uniq = sides.uniq.count
  if n_uniq == 1
    :equilateral
  elsif n_uniq == 2
    :isosceles
  else
    :scalene
  end
end

# Error class used in part 2.  No need to change this code.
class TriangleError < StandardError
end

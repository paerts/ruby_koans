def call_block
  puts 'start of method'
  yield
  yield
  puts 'end of method'
end

call_block {puts "\tin the block"}

=begin
start of method
  in the block
  in the block
end of method
=end

a = 0
call_block do
  a += 10
  puts a
end

=begin
start of method
10
20
end of method
=end


# you can provide parameters to the call to yield
# these will be passed in to the block

def call_block2
  yield('hello', 99)
end

call_block2 {|str, num| puts str + ' ' + num.to_s}

=begin
hello 99
=end
